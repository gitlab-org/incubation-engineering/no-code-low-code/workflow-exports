### What does it do?

The workflow clears the issue's assignee when it is marked as `Done`.

### Source

This workflow is exported from Jira's Settings > System > Global automation screen.

It is visually represented as:

![workflow](/jira/clear-assignee/workflow.png)
