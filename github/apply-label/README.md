### What does it do?

This workflow will triage pull requests and apply a label based on the paths that are modified in the pull request.

### Source

The workflow is a bootstrapped example from [Labeler](https://github.com/marketplace/actions/labeler).

It's worth noting, although the use case is project automation, the syntax is at the lower level compared to the [built-in workflow](/github/project-workflow-item-closed/workflow.json), e.g. the reference of `secrets.GITHUB_TOKEN`.

### Screen

![screen](/github/apply-label/screen.gif)
