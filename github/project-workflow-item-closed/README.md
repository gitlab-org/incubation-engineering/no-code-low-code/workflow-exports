### What does it do?

The workflow is triggered when either PR or Issue is closed then mark the Status field as `Done`.

### Source

The workflow's json representation is intercepted from GitHub's API request when updating/enabling a project's [built-in workflow](https://docs.github.com/en/issues/planning-and-tracking-with-projects/automating-your-project/using-the-built-in-automations).

The workflow is visually rendered as:

![workflow](/github/project-workflow-item-closed/workflow.png)
