### What does it do?

The pipeline builds a python app and deploy via Docker image

### Source

This pipeline is copied from https://github.com/harness-community/python-pipeline-samples

The pipeline is visually rendered in Harness as:

![pipeline](/harness/python-pipeline/pipeline.png)
