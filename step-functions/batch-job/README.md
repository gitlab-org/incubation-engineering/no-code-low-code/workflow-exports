### What does it do?

This workflow (AWS Step Functions) uses the Lambda function to populate the DynamoDB table, uses a for loop to read each of the entries, and then sends each entry to Amazon SQS.

### Source

The workflow is imported from https://docs.aws.amazon.com/step-functions/latest/dg/sample-project-transfer-data-sqs.html

Visualisation from [AWS Step Function Workflow Studio](https://docs.aws.amazon.com/step-functions/latest/dg/workflow-studio.html):

![workflow](/step-functions/batch-job/workflow.png)
